FROM gentoo/portage:latest as portage
FROM gentoo/stage3:amd64-openrc

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

ARG EMERGE_DEFAULT_OPTS=""
ARG FEATURES=""
ARG GLOBAL_USE=""
ARG PORTAGE_BINHOST=""

# emerge --onlydeps will still install xanmod kernel, so installing all its
# dependencies manually
RUN echo "USE=\""${GLOBAL_USE}"\"" >> /etc/portage/make.conf \
    && emerge virtual/libelf \
        sys-apps/coreutils \
        sys-fs/e2fsprogs \
        app-arch/cpio \
        sys-devel/bc \
        sys-devel/flex \
        app-arch/xz-utils \
        dev-vcs/git

COPY make.conf /etc/portage/make.conf
